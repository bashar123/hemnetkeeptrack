package com.example.hemnetkeeptrack

import android.content.Context
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.viewpager.widget.PagerAdapter
import com.bumptech.glide.Glide

class ImageAdapter(private val context: Context, private val imageUrls: ArrayList<String> = ArrayList()) : PagerAdapter() {

//    private var imageUrls  = listOf(
//        "https://bilder.hemnet.se/images/itemgallery_cut/70/23/7023789a2341d54b83fdb0b0a635f8a6.jpg",
//        "https://bilder.hemnet.se/images/itemgallery_cut/15/42/15426292e10c52587cb03f4a7f15cfbd.jpg",
//        "https://bilder.hemnet.se/images/itemgallery_cut/ad/7a/ad7aa16dea9c93cbf9f32b8eb93d15f4.jpg",
//        "https://bilder.hemnet.se/images/itemgallery_cut/98/02/980278f367763abb0148842f802ce35c.jpg")

    override fun isViewFromObject(view: View, `object`: Any): Boolean {
        return view == `object`
    }

    override fun getCount(): Int {
        return imageUrls.size
    }

    override fun instantiateItem(container: ViewGroup, position: Int): Any {
        val imageView = ImageView(context)

        Glide.with(context)
            .load(imageUrls[position])
            .into(imageView)

        imageView.scaleType = ImageView.ScaleType.CENTER_CROP

        container.addView(imageView)

        return imageView
    }

    override fun destroyItem(container: ViewGroup, position: Int, `object`: Any) {
        container.removeView( `object` as ImageView )
    }
}