package com.example.hemnetkeeptrack

import android.os.Bundle
import android.view.MotionEvent
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import com.example.hemnetkeeptrack.fragments.CompareFragment
import com.example.hemnetkeeptrack.fragments.DislikedFragment
import com.example.hemnetkeeptrack.fragments.LikedFragment
import com.google.android.material.bottomnavigation.BottomNavigationView


class MainActivity : AppCompatActivity(){

    private val navigationItemListener  = BottomNavigationView.OnNavigationItemSelectedListener {

        val selectedFragment = when(it.itemId) {
            R.id.nav_compare -> CompareFragment()
            R.id.nav_liked -> LikedFragment()
            R.id.nav_disliked -> DislikedFragment()
            else -> CompareFragment()
        }

        selectFragment(selectedFragment)

        true
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val bottomNavigationView = findViewById<BottomNavigationView>(R.id.bottom_navigation)

        // navigation view listener
        bottomNavigationView.setOnNavigationItemSelectedListener (navigationItemListener)

        // default fragment
        bottomNavigationView.selectedItemId = R.id.nav_compare
    }


    private fun selectFragment(fragment: Fragment){
        supportFragmentManager
            .beginTransaction()
            .replace(R.id.fragment_container, fragment)
            .commit()
    }
}
