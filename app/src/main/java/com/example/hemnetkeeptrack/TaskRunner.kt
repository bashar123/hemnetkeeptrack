package com.example.hemnetkeeptrack

import android.os.AsyncTask

// welcome to callback-hell
interface Tasks<R> {
    fun doInBackground() : R
    fun onPostExecute(result: R)
}

class TaskRunner<R>(private var tasks: Tasks<R>) : AsyncTask<Void?, R, R>() {

    override fun doInBackground(vararg p0: Void?): R {
        return tasks.doInBackground()
    }

    override fun onPostExecute(result: R) {
        tasks.onPostExecute(result)
    }


}