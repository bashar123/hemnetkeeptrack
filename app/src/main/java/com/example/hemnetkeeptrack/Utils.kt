package com.example.hemnetkeeptrack

import android.content.Context
import android.util.Log
import android.widget.Toast


fun log(message: String) {
    Log.d("hemnet-debug", message)
}

fun log2(message: String) {
    Log.d("hemnet-debug-2", message)
}

fun toast(context : Context, message: String){
    Toast.makeText(context, message, Toast.LENGTH_SHORT).show()
}