package com.example.hemnetkeeptrack.client

import com.example.hemnetkeeptrack.log
import com.example.hemnetkeeptrack.models.Apartment
import com.example.hemnetkeeptrack.models.RssApartmentData
import org.json.JSONArray
import org.jsoup.Jsoup
import org.jsoup.nodes.Document
import java.lang.Exception

class HemnetHtmlScraper(private val rssApartments: ArrayList<RssApartmentData>) {

    /**
     * Ranged scrape, null = infinity = rest of un-scraped apartments
     */
    fun scrapeRssApartments(index: Int, count: Int?): ArrayList<Apartment> {

        val startTime = System.nanoTime()

        val result: ArrayList<Apartment> = ArrayList()

        val unScrapedApartments = rssApartments.filter { !it.scraped }

        val size: Int = count ?: unScrapedApartments.size
        val apartmentsToScrape = unScrapedApartments.slice(IntRange(index, size - 1))

        for (rssApartment in apartmentsToScrape) {
            val scrapeResult = scrapeRssApartment(rssApartment)

            if (scrapeResult is ScrapeResultOk) {
                result.add(scrapeResult.apartment)
            } else {
                rssApartments.remove(rssApartment)
            }
        }

        log("Scraping $size apartments took : " + (System.nanoTime() - startTime)/1000000 + " millis")

        return result
    }

    private fun scrapeRssApartment(rssApartment: RssApartmentData): ScrapeResult {
        val apartmentPage: Document = Jsoup
            .connect(rssApartment.link)
            .get()

        if (apartmentIsRemoved(apartmentPage)) {
            return ScrapeResultFailed()
        }

        rssApartment.scraped = true
        val apartmentPrice: Int = scrapeApartmentPrice(apartmentPage, rssApartment.link)
        val apartmentImages: ArrayList<String> = scrapeApartmentImages(apartmentPage, rssApartment.link)

        return ScrapeResultOk(
            Apartment(rssApartment, apartmentPrice, apartmentImages)
        )
    }


    private fun scrapeApartmentPrice(apartmentPage: Document, apartmentLink: String): Int {
        var price = 0
        try {
            price = apartmentPage
                .getElementsByClass("property-info__price")
                .single()
                .text()
                .replace("kr", "", true)
                .replace(" ", "")
                .toInt()
        } catch (_ex: Exception) {
            log("Could not scrape apartments price: $apartmentLink")
        }

        return price
    }


    private fun scrapeApartmentImages(apartmentPage: Document, apartmentLink: String): ArrayList<String> {
        val imageUrls: ArrayList<String> = ArrayList()

        try {
            val htmlImagesContainer = apartmentPage
                .getElementsByClass("carousel js-carousel")
                .single()
                .attr("data-images")

            val images = JSONArray(htmlImagesContainer)

            for (x in 0 until images.length()) {
                val imageUrl = images
                    .getJSONObject(x)
                    .getString("url")

                imageUrls.add(imageUrl)
            }

        } catch (_ex: Exception) {
            log("Could not scrape apartments images: $apartmentLink")
        }

        return imageUrls
    }


    private fun apartmentIsRemoved(apartmentPage: Document): Boolean {
        return apartmentPage
            .getElementsByClass("removed-property")
            .size == 1
    }
}