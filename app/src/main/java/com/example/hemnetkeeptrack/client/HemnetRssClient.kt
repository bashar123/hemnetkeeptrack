package com.example.hemnetkeeptrack.client

import com.example.hemnetkeeptrack.models.RssApartmentData
import org.jsoup.Jsoup
import org.jsoup.nodes.Document
import org.jsoup.parser.Parser
import java.net.URL

class HemnetRssClient(rssFeedUrl: String) {

    private val rssDocument: Document = Jsoup.parse(URL(rssFeedUrl).openStream(), "UTF-8", "", Parser.xmlParser())
    private val rssTitle: String // todo public when needed

    init {
        rssTitle = rssDocument
            .select("channel")
            .single()
            .children()
            .first{e -> e.tagName() == "title"}
            .text()
    }

    fun parseRssApartments(): ArrayList<RssApartmentData>{
        val rssApartments: ArrayList<RssApartmentData> = ArrayList()
        val apartmentItems = rssDocument.select("item")

        for(i in  0 until apartmentItems.size){
            val apartmentItem = apartmentItems[i]

            rssApartments.add(
                RssApartmentData(
                    apartmentItem.select("title").single().text(),
                    apartmentItem.select("description").single().text(),
                    apartmentItem.select("pubDate").single().text(),
                    apartmentItem.select("link").single().text()
                )
            )
        }

        return rssApartments
    }

}