package com.example.hemnetkeeptrack.client

import com.example.hemnetkeeptrack.models.Apartment

open class ScrapeResult

class ScrapeResultOk(val apartment: Apartment) : ScrapeResult()

class ScrapeResultFailed: ScrapeResult()

