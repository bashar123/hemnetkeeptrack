package com.example.hemnetkeeptrack.custom_views

import android.content.Context
import android.util.AttributeSet
import android.webkit.WebView

class HeadlessWebView : WebView {

    constructor(context: Context?) : super(context) { init() }
//    constructor(context: Context?, attrs: AttributeSet?) : super(context, attrs){init()}
//    constructor(context: Context?, attrs: AttributeSet?, defStyleAttr: Int) : super(context, attrs, defStyleAttr){init()}


    private fun init() {
        settings.javaScriptEnabled = true

    }



}