package com.example.hemnetkeeptrack.fragments

import android.os.Bundle
import android.os.Message
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageButton
import android.widget.ImageView
import android.widget.ProgressBar
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.viewpager.widget.ViewPager
import com.example.hemnetkeeptrack.*
import com.example.hemnetkeeptrack.client.HemnetHtmlScraper
import com.example.hemnetkeeptrack.client.HemnetRssClient
import com.example.hemnetkeeptrack.models.Apartment
import com.google.android.material.dialog.MaterialAlertDialogBuilder

// TODO ROTATE TO VIEW FULL SIZE IMAGES
class CompareFragment : Fragment() {

    private var myProgressBar: ProgressBar? = null
    private var myViewPager: ViewPager? = null
    private var apartmentDescriptionButton: ImageButton? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_compare, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)



        TaskRunner(object : Tasks<ArrayList<Apartment>> {

            override fun doInBackground(): ArrayList<Apartment> {
                val hrc = HemnetRssClient("https://www.hemnet.se/mitt_hemnet/sparade_sokningar/20045869.xml")
                val rssApartments = hrc.parseRssApartments()
                val scraper = HemnetHtmlScraper(rssApartments)

                return scraper.scrapeRssApartments(0, 1)
            }

            override fun onPostExecute(apartments: ArrayList<Apartment>) {
                myProgressBar = view.findViewById(R.id.progress_bar)
                myProgressBar?.visibility = ProgressBar.INVISIBLE

                myViewPager = view.findViewById(R.id.images_view_pager)
                myViewPager?.adapter = ImageAdapter(context!!, apartments.first().imageUrls)

                val placeholder = view.findViewById<ImageView>(R.id.placeholder)
                placeholder?.visibility = ImageView.INVISIBLE

                val title = view.findViewById<TextView>(R.id.apartment_title)
                title.text = apartments.first().rssApartment.title

                setupApartmentDescriptionButton(view, apartments.first().rssApartment.description)
            }

        }).execute()
    }


    private fun setupApartmentDescriptionButton(view: View, message: String) {
        apartmentDescriptionButton = view.findViewById(R.id.apartment_description)
        apartmentDescriptionButton?.setOnClickListener { _ ->
            MaterialAlertDialogBuilder(context)
                .setTitle("Mer info om lägenheten")
                .setMessage(message)
                .setPositiveButton("Ok") { _, _ ->
                    log("ok")
                }
//                .setNegativeButton("Count") { _, _ ->
//                    toast(context!!,"ok$size")
//                }
                .show()
        }
    }


    override fun onDestroyView() {
        log("CompareFragment -> onDestroyView")
        super.onDestroyView()
    }
}