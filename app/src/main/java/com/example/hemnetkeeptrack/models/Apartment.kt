package com.example.hemnetkeeptrack.models

import kotlin.collections.ArrayList

/**
 * Complete apartment.
 * Todo : add dictionary of available information-values
 */
class Apartment (
    val rssApartment: RssApartmentData,
    var price: Int,
    val imageUrls: ArrayList<String>
)