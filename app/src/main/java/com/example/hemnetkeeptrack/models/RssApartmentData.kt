package com.example.hemnetkeeptrack.models

class RssApartmentData (
    val title: String,
    val description: String,
    val publicationDate: String, // todo Date()
    val link: String
){
    var scraped: Boolean = false
}